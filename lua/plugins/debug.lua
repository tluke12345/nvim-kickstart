-- debug.lua
--
-- Shows how to use the DAP plugin to debug your code.
--
-- Primarily focused on configuring the debugger for Go, but can
-- be extended to other languages as well. That's why it's called
-- kickstart.nvim and not kitchen-sink.nvim ;)

return {
  'mfussenegger/nvim-dap',
  dependencies = {
    -- Creates a beautiful debugger UI
    { 'rcarriga/nvim-dap-ui', dependencies = { 'nvim-neotest/nvim-nio' } },

    -- Installs the debug adapters for you
    'williamboman/mason.nvim',
    'jay-babu/mason-nvim-dap.nvim',

    -- Add your own debuggers here

    -- Go debugger
    -- 'leoluz/nvim-dap-go',
  },
  config = function()
    local dap = require 'dap'
    local dapui = require 'dapui'

    require('mason-nvim-dap').setup {
      automatic_installation = true,
      -- You can provide additional configuration to the handlers,
      handlers = {},
      ensure_installed = {
        -- Update this to ensure that you have the debuggers for the langs you want
        -- 'delve', -- Go
      },
    }

    -- Basic debugging keymaps, feel free to change to your liking!
    vim.keymap.set('n', '<leader>dc', dap.continue, { desc = 'Debug: [C]ontinue/Start' })
    vim.keymap.set('n', '<leader>dq', dap.terminate, { desc = 'Debug: [Q]uit' })
    vim.keymap.set('n', '<leader>di', dap.step_into, { desc = 'Debug: [I]nto (<F1>)' })
    vim.keymap.set('n', '<leader>dn', dap.step_over, { desc = 'Debug: [N]ext (<F2>)' })
    vim.keymap.set('n', '<leader>do', dap.step_out, { desc = 'Debug: [O]ut (<F3>)' })

    vim.keymap.set('n', '<F1>', dap.step_over, { desc = 'Debug: Step Into' })
    vim.keymap.set('n', '<F2>', dap.step_over, { desc = 'Debug: Step Over' })
    vim.keymap.set('n', '<F3>', dap.step_out, { desc = 'Debug: Step Out' })

    vim.keymap.set('n', '<leader>db', dap.toggle_breakpoint, { desc = 'Debug: [B]reakpoint  (toggle)' })
    vim.keymap.set('n', '<leader>dB', function()
      dap.set_breakpoint(vim.fn.input 'Breakpoint condition:')
    end, { desc = 'Debug: [B]reakpoint (condition) ' })

    -- Dap UI setup
    -- For more information, see |:help nvim-dap-ui|
    dapui.setup()

    -- Toggle to see last session result. Without this, you can't see session output in case of unhandled exception.
    vim.keymap.set('n', '<leader>dt', dapui.toggle, { desc = 'Debug: [T]oggle UI.' })
    vim.keymap.set('n', '<leader>dr', function()
      dapui.close()
      dapui.open { reset = true }
    end, { desc = 'Debug: [R]eset UI.' })

    dap.listeners.before.attach.dapui_config = function()
      dapui.open()
    end
    dap.listeners.before.launch.dapui_config = function()
      dapui.open()
    end
    dap.listeners.before.event_terminated.dapui_config = function()
      dapui.close()
    end
    dap.listeners.before.event_exited.dapui_config = function()
      dapui.close()
    end

    -- Install golang specific config
    -- require('dap-go').setup()
  end,
}
