local utility = require "utility"
-- Collection of various small independent plugins/modules
return {
  'echasnovski/mini.nvim',
  config = function()
    -- Better Around/Inside textobjects
    --
    -- Examples:
    --  - va)  - [V]isually select [A]round [)]paren
    --  - yinq - [Y]ank [I]nside [N]ext [']quote
    --  - ci'  - [C]hange [I]nside [']quote
    require('mini.ai').setup { n_lines = 500 }

    -- Add/delete/replace surroundings (brackets, quotes, etc.)
    --
    -- - saiw) - [S]urround [A]dd [I]nner [W]ord [)]Paren
    -- - sd'   - [S]urround [D]elete [']quotes
    -- - sr)'  - [S]urround [R]eplace [)] [']
    require('mini.surround').setup()

    -- Simple and easy statusline.
    --  You could remove this setup call if you don't like it,
    --  and try some other statusline plugin
    local statusline = require 'mini.statusline'
    statusline.setup()

    -- You can configure sections in the statusline by overriding their
    -- default behavior. For example, here we disable the section for
    -- cursor information because line numbers are already enabled
    ---@diagnostic disable-next-line: duplicate-set-field
    statusline.section_location = function()
      return ''
    end


    if utility.is_xterm_16color() then
      vim.cmd 'colorscheme minischeme'
    end

    -- ... and there is more!
    --  Check out: https://github.com/echasnovski/mini.nvim
    -- require('mini.base16').setup {
    --   palette = {
    --     base00 = '#1E2127', -- background
    --     base01 = '#2f3237', -- brightBlack (gutters, selected line)
    --     base02 = '#33331b', -- brightGreen (visual mode selection, current selection)
    --     base03 = '#697180', -- foreground (notes, dimmed text)
    --     base04 = '#D19A66', -- brightYellow (numbers)
    --     base05 = '#d1e8fa', -- brightBlue (paths, variable names, normal mode status line bg)
    --     base06 = '#C678DD', -- brightMagenta ???
    --     base07 = '#FFffff', -- brightWhite ???
    --     base08 = '#E06C75', -- red
    --     base09 = '#D19A66', -- yellow
    --     base0A = '#98C379', -- green
    --     base0B = '#56B6C2', -- cyan
    --     base0C = '#61AFEF', -- blue
    --     base0D = '#C678DD', -- purple
    --     base0E = '#E06c75', -- brightRed (functions)
    --     base0F = '#56B6C2', -- brightCyan (strings)
    --
    --     -- purple : directorie
    --     -- blue : info notification
    --     -- pink? warnings?
    --     -- red: errors
    --   },
    -- }
  end,
}
