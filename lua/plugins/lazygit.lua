-- LazyGit
-- Popup window for total git management
return {
  {
    'kdheepak/lazygit.nvim',
    -- optional for floating window border decoration
    dependencies = {
      'nvim-lua/plenary.nvim',
    },
    config = function()
      vim.keymap.set('n', '<leader>gg', '<Cmd>LazyGit<CR>', { desc = 'LazyGit' })
    end,
    cond = function()
      local git_dir = vim.fn.finddir('.git', vim.fn.expand '%:p:h' .. ';')
      return git_dir ~= ''
    end,
  },
}
