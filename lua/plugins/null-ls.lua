return {
  {
    'jay-babu/mason-null-ls.nvim',
    event = { 'BufReadPre', 'BufNewFile' },
    dependencies = {
      'williamboman/mason.nvim',
      'nvimtools/none-ls.nvim',
      'neovim/nvim-lspconfig', -- Initial mason setup happens here.
    },
    config = function()
      require('mason-null-ls').setup {
        -- LSP Languages are set in lsp.lua
        -- Here are just linting/diagnostics/formatters
        ensure_installed = {
          'eslint',
          'prettierd',
          'markdownlint',
          'jsonlint',
          'yamllint',
          -- 'shellcheck',
          'markdownlint',
        },
        automatic_installation = true,
        handlers = {

          prettierd = function(params)
            return {
              command = 'prettierd',
              args = { '--stdin' },
              writer = params.content,
              read_stderr = true,
            }
          end,

          markdownlint = function(params)
            local null_ls = require 'null-ls'
            null_ls.register(null_ls.builtins.diagnostics.markdownlint.with {
              extra_args = { '--disable', 'MD033', '--' },
            })
          end,
        },
      }
      local null_ls = require 'null-ls'
      null_ls.setup {
        sources = {
          null_ls.builtins.formatting.prettierd.with {
            extra_filetypes = { 'svg' },
          },
        },

        on_attach = function(client, bufnr)
          -- Format on save
          if client.supports_method 'textDocument/formatting' then
            local augroup = vim.api.nvim_create_augroup('LspFormatting', {})
            vim.api.nvim_clear_autocmds { group = augroup, buffer = bufnr }
            vim.api.nvim_create_autocmd('BufWritePre', {
              group = augroup,
              buffer = bufnr,
              callback = function()
                vim.lsp.buf.format { async = false }
              end,
            })
          end
        end,
      }

      vim.keymap.set('n', '<leader>wn', '<cmd>NullLsInfo<CR>', { desc = 'Workspace: [N]ullLs info' })
    end,
  },
}
