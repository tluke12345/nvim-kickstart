local utility = require 'utility'
local color = require 'color_util'

-- No support for true color
if utility.is_xterm_16color() then
  return {}
end

-- Toggle onedark mode
local toggleOneDarkMode = function()
  require('onedark').toggle()
end

-- My colors
local c = {
  -- 'high contrast' colors taken from onedarkhc
  -- (onedarkh did not have markdown support! and, after looking at the
  -- code, ondark is considerably more complete!)
  red = '#ff6e7a',
  dark_red = '#BE5046',
  green = '#c0fa96',
  yellow = '#ffd587',
  dark_yellow = '#D19A66',
  blue = '#6ebeff',
  purple = '#e387ff',
  cyan = '#66deed',
  -- white = '#dfe5f2',
  -- black = '#21242b',
  -- visual_black = 'NONE',
  -- comment_grey = '#5C6370',
  -- gutter_fg_grey = '#4B5263',
  -- cursor_grey = '#2C323C',
  -- visual_grey = '#3E4452',
  -- menu_grey = '#3E4452',
  -- special_grey = '#3B4048',
  -- vertsplit = '#181A1F',

  -- others
  fg = '#c2c6cf',
  bg0 = '#1b1c1e',
  bg1 = '#2c2d31',
  bg2 = '#414136',
  grey = '#63625f',
  status_filename = '#b3b7c0',
}

return {
  'navarasu/onedark.nvim',
  lazy = false,
  priority = 1000,
  opts = {
    style = 'warmer',
    toggle_style_list = { 'darker', 'deep', 'warmer' },
    colors = {
      black = '#101012',
      bg0 = c.bg0, -- editor background
      bg1 = c.bg1, -- highlighted (gutters, selections, folds), statusbar bg
      bg2 = c.bg2, -- highlighted words
      bg3 = '#37383d',
      bg_d = '#1b1c1e',
      bg_blue = '#68aee8',
      bg_yellow = '#e2c792',
      fg = c.fg, --'#a7aab0',
      purple = c.purple,
      green = c.green,
      orange = '#c49060',
      blue = c.blue,
      yellow = c.yellow,
      cyan = c.cyan,
      red = c.red,
      grey = c.grey, -- de-emphasised fg (line numbers, comments, status fg)
      light_grey = '#818387',
      dark_cyan = '#2b5d63',
      dark_red = c.dark_red,
      dark_yellow = c.dark_yellow,
      dark_purple = '#79428a',
      diff_add = '#282b26',
      diff_delete = '#2a2626',
      diff_change = '#1a2a37',
      diff_text = '#2c485f',
    },

    -- here you can customize how the colors are applied
    highlights = {
      -- ??
      CursorColumn = { bg = c.red },

      -- 80, 120 char column indicator line color
      -- (defaults bg1)
      ColorColumn = { bg = color.lighten(c.bg0, 0.99, c.white) },

      -- Fold column (left of the gitsign column)
      FoldColumn = { bg = c.bg0, fg = c.fg },

      -- status bar (mini) filename is too hard to read!
      MiniStatuslineFilename = { fg = c.status_filename, bg = c.bg1 },
    },
  },
  config = function(_, opts)
    -- onedarkhc
    -- vim.cmd.colorscheme 'onedarkhc'

    -- onedark
    require('onedark').setup(opts)
    vim.keymap.set('n', '<leader>wo', toggleOneDarkMode, { desc = 'Workspace: [O]neDark Mode (toggle)' })
    vim.cmd.colorscheme 'onedark'
  end,
}

-- onedarkhc
-- 'pacokwon/onedarkhc.vim',
-- vim.cmd.colorscheme 'onedarkhc'
