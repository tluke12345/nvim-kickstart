-- Fuzzy Finder (files, lsp, etc)
return {
  'nvim-telescope/telescope.nvim',
  event = 'VimEnter',
  branch = '0.1.x',
  dependencies = {
    'nvim-lua/plenary.nvim',
    { -- If encountering errors, see telescope-fzf-native README for install instructions
      'nvim-telescope/telescope-fzf-native.nvim',
      build = 'make',
      -- `cond` - determine whether this plugin should be
      cond = function()
        return vim.fn.executable 'make' == 1
      end,
    },
    { 'nvim-telescope/telescope-ui-select.nvim' },

    --  If you already have a Nerd Font you can use this
    { 'nvim-tree/nvim-web-devicons' },
  },
  config = function()
    -- Two important keymaps to use while in telescope are:
    --  - Insert mode: <c-/>
    --  - Normal mode: ?

    -- [[ Configure Telescope ]]
    -- See `:help telescope` and `:help telescope.setup()`
    require('telescope').setup {
      -- defaults = {
      --   mappings = {
      --     i = { ['<c-enter>'] = 'to_fuzzy_refine' },
      --   },
      -- },
      -- pickers = {}
      extensions = {
        ['ui-select'] = {
          require('telescope.themes').get_dropdown(),
        },
      },
    }

    -- Enable telescope extensions, if they are installed
    pcall(require('telescope').load_extension, 'fzf')
    pcall(require('telescope').load_extension, 'ui-select')

    -- See `:help telescope.builtin`
    local builtin = require 'telescope.builtin'

    -- SEARCHING
    vim.keymap.set('n', '<leader>ss', builtin.grep_string, { desc = 'Search Text: [S]elected Word' })
    vim.keymap.set('n', '<leader>sw', builtin.live_grep, { desc = 'Search Text: [W]orkspace' })
    vim.keymap.set('n', '<leader>sr', builtin.resume, { desc = 'Search Text: [R]esume previous search' })
    vim.keymap.set('n', '<leader>sh', builtin.help_tags, { desc = 'Workspace: [H]elp (vim)' })

    -- WORKSPACE
    vim.keymap.set('n', '<leader>wk', builtin.keymaps, { desc = 'Workspace: [K]eymaps' })
    vim.keymap.set('n', '<leader>wt', builtin.builtin, { desc = 'Workspace: [T]elescope builtins' })

    -- FILESYSTEM
    local function find_all()
      builtin.find_files { no_ignore = true }
    end
    vim.keymap.set('n', '<leader>fa', find_all, { desc = 'Files: [A]ll Files' })
    vim.keymap.set('n', '<leader>ff', builtin.find_files, { desc = 'Files: [F]iles (.gitignore)' })
    vim.keymap.set('n', '<leader>fr', builtin.oldfiles, { desc = 'Files: [R]ecent Files' })
    vim.keymap.set('n', '<leader>fb', builtin.buffers, { desc = 'Files: [B]uffers' })

    local function nvim_config()
      builtin.find_files { cwd = vim.fn.stdpath 'config' }
    end
    local function notes()
      builtin.find_files { cwd = '~/notes' }
    end
    local function linux_config()
      builtin.find_files { cwd = '~/.config' }
    end
    local function home()
      builtin.find_files { cwd = '~' }
    end

    vim.keymap.set('n', '<leader>fv', nvim_config, { desc = 'Files: [V]im (config)' })
    vim.keymap.set('n', '<leader>fn', notes, { desc = 'Files: [N]otes' })
    vim.keymap.set('n', '<leader>fc', linux_config, { desc = 'Files: [C]onfig (linux .config)' })
    vim.keymap.set('n', '<leader>fh', home, { desc = 'Files: [H]ome directory' })
  end,
}
