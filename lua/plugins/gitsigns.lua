-- Gitsigns: Inline Git annotations and hunk management
-- See `:help gitsigns`
--
-- NOTE: many keymaps assigned in keymaps.lua
return {
  'lewis6991/gitsigns.nvim',
  opts = {
    signs = {
      add = { text = '+' },
      change = { text = '~' },
      delete = { text = '_' },
      topdelete = { text = '‾' },
      changedelete = { text = '┆' },
    },
  },
}
