return {
  'mbbill/undotree',
  config = function()
    vim.keymap.set('n', '<leader>wu', '<cmd>UndotreeToggle<cr>', { desc = 'Workspace: [U]ndoTree (toggle)' })

    -- options

    -- Window layout
    -- 1: left, small diff
    -- 2: left, big diff
    -- 3: right, small diff
    -- 4: right, big diff
    vim.g.undotree_WindowLayout = 3

    vim.g.undotree_ShortIndicators = 1
    -- vim.g.undotree_SplitWidth = 30
    vim.g.undotree_DiffAutoOpen = 0
    -- vim.g.undotree_SetFocusWhenToggle = 0
    -- vim.g.undotree_TreeNodeShape = '*'
    -- vim.g.undotree_TreeVertShape = '|'
    -- vim.g.undotree_TreeSplitShape = '/'
    -- vim.g.undotree_TreeReturnShape = '\\'
    -- vim.g.undotree_DiffCommand = "diff"
    -- vim.g.undotree_RelativeTimestamp = 1
    -- vim.g.undotree_HighlightChangedText = 1
    -- vim.g.undotree_HighlightChangedWithSign = 1
    -- vim.g.undotree_HighlightSyntaxAdd = "DiffAdd"
    -- vim.g.undotree_HighlightSyntaxChange = "DiffChange"
    -- vim.g.undotree_HighlightSyntaxDelete = "DiffDelete"
    -- vim.g.undotree_HelpLine = 1
    -- vim.g.undotree_CursorLine = 1
    -- vim.g.undotree_UndoDir =

    -- commands:
    -- UndotreeToggle
    -- UndotreeHide
    -- UndotreeShow
    -- UndotreeFocus
    -- UndotreePersistUndo
  end,
}
