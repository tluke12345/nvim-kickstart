-- Terminal session manager
-- NOTE: for terminal keybindings, see keymaps.lua
--
-- toggle terminal        -> <C-t>
--
-- Multiple terminals
-- current (defaults to 1)          -> <C-t>
-- term 1                           -> 1<C-t>
-- term 2                           -> 2<C-t>
-- term 3                           -> 3<C-t>
-- ...
--
-- two terminal splits
--   1. open terminal               -> <C-t>
--   2. exit to normal mode         -> <C-\>
--   3. start a new terminal        -> :2ToggleTerm
--   (4). Reset term splits         -> :ToggleTermToggleAll
--
-- Other
--   - open specific term           -> :TermSelect (opens picker)
--   - resize                       -> <C-up,down,left,right>
--   - send selection to term       -> :ToggleTermSendVisualSelection [term id]
--     * this adds <CR>... better control available with lua api
return {
  {
    'akinsho/toggleterm.nvim',
    version = '*',
    opts = {
      open_mapping = [[<c-t>]],
      size = 20,
      on_open = function()
        vim.opt.foldcolumn = '0'
        vim.opt.signcolumn = 'no'
        vim.opt.cursorline = false
      end,
    },
  },
}
