-- LSD Configuration

-- ----------------------------------------------------------------------------
-- Ensure Installed
--
-- this function returns a list of LSPs that should be installed
local function ensure_installed()
  return {
    -- Lua
    'stylua', -- Used to format lua code

    -- GO
    -- 'gopls',
    -- 'goimports',
    -- 'gofumpt',
    -- 'templ',
    -- 'html',

    -- TypeScript
    -- 'typescript-language-server',
    -- 'eslint-lsp',
  }
end

-- ----------------------------------------------------------------------------
-- Configure Languages
-- See `:help lspconfig-all` for a list of all the pre-configured LSPs
--
--  Add any additional override configuration in the following tables. Available keys are:
--  - cmd (table): Override the default command used to start the server
--  - filetypes (table): Override the default list of associated filetypes for the server
--  - capabilities (table): Override fields in capabilities. Can be used to disable certain LSP features.
--  - settings (table): Override the default settings passed when initializing the server.
--        For example, to see the options for `lua_ls`, you could go to: https://luals.github.io/wiki/settings/
local function language_configurations()
  return {
    -- clangd = {},
    -- pyright = {},
    -- rust_analyzer = {},

    -- If custom configuration is not needed, set {}

    -- GO
    -- gopls = {},
    -- goimports = {},
    -- gofumpt = {},
    -- templ = {},
    -- html = {
    --   filetypes = { 'html', 'templ' },
    -- },

    -- Sometimes cusome configuration is needed
    lua_ls = {
      -- cmd = {...},
      -- filetypes { ...},
      -- capabilities = {},
      settings = {
        Lua = {
          runtime = { version = 'LuaJIT' },
          workspace = {
            checkThirdParty = false,
            -- Tells lua_ls where to find all the Lua files that you have loaded
            -- for your neovim configuration.
            library = {
              '${3rd}/luv/library',
              unpack(vim.api.nvim_get_runtime_file('', true)),
            },
            -- If lua_ls is really slow on your computer, you can try this instead:
            -- library = { vim.env.VIMRUNTIME },
          },
          completion = {
            callSnippet = 'Replace',
          },
          -- You can toggle below to ignore Lua_LS's noisy `missing-fields` warnings
          -- diagnostics = { disable = { 'missing-fields' } },
        },
      },
    },
  }
end

-- ----------------------------------------------------------------------------
-- The following two autocommands are used to highlight references of the
-- word under your cursor when your cursor rests there for a little while.
--    See `:help CursorHold` for information about when this is executed
--
-- When you move your cursor, the highlights will be cleared (the second autocommand).
local function highlight_references(event)
  local client = vim.lsp.get_client_by_id(event.data.client_id)
  if client and client.server_capabilities.documentHighlightProvider then
    vim.api.nvim_create_autocmd({ 'CursorHold', 'CursorHoldI' }, {
      buffer = event.buf,
      callback = vim.lsp.buf.document_highlight,
    })

    vim.api.nvim_create_autocmd({ 'CursorMoved', 'CursorMovedI' }, {
      buffer = event.buf,
      callback = vim.lsp.buf.clear_references,
    })
  end
end

-- ----------------------------------------------------------------------------
-- This function gets run when an LSP attaches to a particular buffer.
-- keymaps are only set when LSP is available
local function configure_buffer(event)
  -- helper
  local map = function(keys, func, desc)
    vim.keymap.set('n', keys, func, { buffer = event.buf, desc = desc })
  end

  map('gd', require('telescope.builtin').lsp_definitions, '[G]oto [D]efinition')
  map('gr', require('telescope.builtin').lsp_references, '[G]oto [R]eferences')
  map('gI', require('telescope.builtin').lsp_implementations, '[G]oto [I]mplementation')
  map('<leader>ls', require('telescope.builtin').lsp_document_symbols, 'LSP: [s]ymbols (current file)')
  map('<leader>lS', require('telescope.builtin').lsp_dynamic_workspace_symbols, 'LSP: [S]ymbols (workspace)')
  map('<leader>lr', vim.lsp.buf.rename, 'LSP: [R]ename')
  map('<leader>la', vim.lsp.buf.code_action, 'LSP: [A]ction (code action)')
  map('K', vim.lsp.buf.hover, 'Hover Documentation')
  map('gD', vim.lsp.buf.declaration, '[G]oto [D]eclaration')

  -- Diagnostics
  map('[d', vim.diagnostic.goto_prev, 'Go to previous [D]iagnostic message')
  map(']d', vim.diagnostic.goto_next, 'Go to next [D]iagnostic message')
  map('<leader>le', vim.diagnostic.open_float, 'LSP: [E]rrors')
  map('<leader>ld', require('telescope.builtin').diagnostics, 'Lsp: [D]iagnostics')
end

-- ----------------------------------------------------------------------------
-- LSP lazy table
-- ----------------------------------------------------------------------------
return {
  { -- LSP Configuration & Plugins
    'neovim/nvim-lspconfig',
    dependencies = {
      -- Automatically install LSPs and related tools to stdpath for neovim
      'williamboman/mason.nvim',
      'williamboman/mason-lspconfig.nvim',
      'WhoIsSethDaniel/mason-tool-installer.nvim',

      -- Useful status updates for LSP.
      { 'j-hui/fidget.nvim', opts = {} },
    },
    config = function()
      --  This function gets run when an LSP attaches to a particular buffer.
      vim.api.nvim_create_autocmd('LspAttach', {
        group = vim.api.nvim_create_augroup('kickstart-lsp-attach', { clear = true }),
        callback = function(event)
          configure_buffer(event)
          highlight_references(event)
        end,
      })

      --  Extend neoVim's capabilities
      local capabilities = vim.lsp.protocol.make_client_capabilities()
      capabilities = vim.tbl_deep_extend('force', capabilities, require('cmp_nvim_lsp').default_capabilities())

      -- Enable the following language servers
      local servers = language_configurations()
      local ensured_list = vim.tbl_keys(servers or {})
      vim.list_extend(language_configurations() or {}, ensure_installed() or {})

      -- Setup LSP-Mason integration
      require('mason').setup()
      require('mason-tool-installer').setup { ensure_installed = ensured_list }
      require('mason-lspconfig').setup {
        handlers = {
          function(server_name)
            local server = servers[server_name] or {}
            server.capabilities = vim.tbl_deep_extend('force', {}, capabilities, server.capabilities or {})
            require('lspconfig')[server_name].setup(server)
          end,
        },
      }
    end,

    -- Post-Install

    -- go
    -- vim.filetype.add { extension = { templ = 'templ' } },
    -- vim.api.nvim_create_autocmd({ 'BufWritePre' }, { pattern = { '*.templ' }, callback = vim.lsp.buf.format }),
  },
}
