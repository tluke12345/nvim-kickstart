local ls = require 'luasnip'
local snippet = ls.snippet
--[[ local snippet_node = ls.snippet_node ]]
local text = ls.text_node
local insert = ls.insert_node
--[[ local choice = ls.choice_node ]]
--[[ local func = ls.?? ]]
local fmt = require('luasnip.extras.fmt').fmt

ls.add_snippets('javascript', {
  snippet('log', {
    text "console.log('",
    insert(1, 'message'),
    text "');",
  }),
})

ls.add_snippets('go', {
  snippet(
    'errcheck',
    fmt(
      [[
        if err != nil {{
            {}
        }}
    ]],
      { insert(1, '/* handle error */') }
    )
  ),
})
