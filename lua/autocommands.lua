-- [[ Basic Autocommands ]]
--  See `:help lua-guide-autocommands`

-- Highlight when yanking (copying) text
--  Try it with `yap` in normal mode
--  See `:help vim.highlight.on_yank()`
vim.api.nvim_create_autocmd('TextYankPost', {
  desc = 'Highlight when yanking (copying) text',
  group = vim.api.nvim_create_augroup('kickstart-highlight-yank', { clear = true }),
  callback = function()
    vim.highlight.on_yank()
  end,
})

-- Auto-saving
-- Saves WITHOUT formatting ('noa') 1 second after leaving insert mode
local autosave_timer = vim.loop.new_timer()
local delay_ms = 1000 -- Delay in milliseconds
vim.api.nvim_create_autocmd({ 'InsertLeave', 'TextChanged' }, {
  pattern = '*',
  callback = function()
    autosave_timer:stop() -- Stop the timer to prevent multiple saves
    autosave_timer:start(
      delay_ms,
      0,
      vim.schedule_wrap(function()
        if vim.fn.getbufvar(vim.fn.bufnr(), '&mod') == 1 then
          vim.cmd 'silent! noa update'
          --require 'notify' ('Auto-saved', 'info')
        end
      end)
    )
  end,
})

-- Enter insert mode automatically when entering a terminal buffer
-- ----------------------------------------------------------------------------
-- Note: none of these catch mouse click events
-- FocusGained:                When the terimaal application is focused
-- BufEnter:                   When entering buffer
-- TermEnter:                  When entering a terminal buffer
vim.api.nvim_create_autocmd('BufEnter', {
  pattern = '*',
  callback = function()
    if vim.bo.filetype == 'toggleterm' then
      vim.cmd 'startinsert'
    else
      vim.cmd 'stopinsert'
    end
  end,
})
