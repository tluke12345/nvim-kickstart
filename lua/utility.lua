local M = {}

M.toggle_relative_numbers = function()
  if vim.o.relativenumber == true then
    vim.o.relativenumber = false
  else
    vim.o.relativenumber = true
  end
end

M.listed_buffers = function()
  local all_buffers = vim.api.nvim_list_bufs()
  local listed = {}
  for _, bufnr in ipairs(all_buffers) do
    if vim.bo[bufnr].buflisted == true then
      table.insert(listed, bufnr)
    end
  end
  return listed
end

M.kill_buffer = function()
  local bufnr = vim.api.nvim_get_current_buf()
  if vim.bo[bufnr].buflisted == false then
    return
  end

  local buffer_count = #M.listed_buffers()
  if buffer_count == 0 then
    print 'No buffers'
  elseif buffer_count == 1 then
    vim.cmd 'bd'
  else
    vim.cmd 'bp | bd #'
  end
end

M.is_xterm_16color = function()
  return os.getenv 'TERM' == 'xterm-16color'
end

M.is_wsl = function()
  return os.getenv 'WSL_DISTRO_NAME' ~= nil
end

M.is_iterm = function()
  return os.getenv 'TERM_PROGRAM' == 'iTerm.app'
end

-- plugins/comment.lua
M.toggleComment = "<cmd>lua require('Comment.api').toggle.linewise.count(1)<CR>"
M.toggleCommentBlock = "<esc><cmd>lua require('Comment.api').toggle.linewise(vim.fn.visualmode())<cr>"

return M
