local util = require 'utility'

-- HELP
--  See `:help vim.keymap.set()`
--`:help wincmd` for a list of all window commands

-- ----------------------------------------------------------------------------
-- VANILLA
-- ----------------------------------------------------------------------------

-- highlight
-- Set highlight on search, but clear on pressing <Esc> in normal mode
vim.opt.hlsearch = true
vim.keymap.set('n', '<Esc>', '<cmd>nohlsearch<CR>')

-- window
vim.keymap.set('n', '<C-h>', '<C-w><C-h>', { desc = 'Move focus to the left window' })
vim.keymap.set('n', '<C-l>', '<C-w><C-l>', { desc = 'Move focus to the right window' })
vim.keymap.set('n', '<C-j>', '<C-w><C-j>', { desc = 'Move focus to the lower window' })
vim.keymap.set('n', '<C-k>', '<C-w><C-k>', { desc = 'Move focus to the upper window' })

-- split resizing
vim.keymap.set('n', '<C-S-Left>', '<Cmd>vertical resize -5<CR>', { desc = 'decrease window height' })
vim.keymap.set('n', '<C-S-Right>', '<Cmd>vertical resize +5<CR>', { desc = 'increase window height' })
vim.keymap.set('n', '<C-S-Down>', '<Cmd>horizontal resize -2<CR>', { desc = 'decrease window width' })
vim.keymap.set('n', '<C-S-Up>', '<Cmd>horizontal resize +2<CR>', { desc = 'increase window width' })
vim.keymap.set('n', '<leader>ws', '<Cmd>vsplit<CR>', { desc = 'Workspace: [S]plit (vertical)' })

-- center screen after page up/down
vim.keymap.set('n', 'n', 'nzzzv', { desc = 'Next Match' })
vim.keymap.set('n', 'N', 'Nzzzv', { desc = 'Prev Match' })
vim.keymap.set('n', '<C-d>', '<C-d>zz', { desc = 'Move down half a screen' })
vim.keymap.set('n', '<C-u>', '<C-u>zz', { desc = 'Move up half a screen' })

-- buffers
vim.keymap.set('n', '<leader>c', util.kill_buffer, { desc = 'Buffer: [c]lose' })
vim.keymap.set('n', '<leader>C', '<cmd>%bd<CR>', { desc = 'Buffer: [C]lose all' })
vim.keymap.set('n', '<leader>X', '<cmd>%bd | e# | bd#<CR>', { desc = 'Buffer: Close all e[X]cept current' })
vim.keymap.set('n', 'H', '<cmd>bp<CR>', { desc = 'Next buffer' })
vim.keymap.set('n', 'L', '<cmd>bn<CR>', { desc = 'Prev buffer' })

-- quit/save
vim.keymap.set('n', '<C-s>', '<cmd>w<cr>', { desc = 'Save' })
vim.keymap.set('n', '<C-q>', '<cmd>qa<cr>', { desc = 'quit' })
vim.keymap.set('n', '<C-w>', '<cmd>q<cr>', { desc = 'Close window' })

-- univeral copy and paste
vim.keymap.set('v', '<leader>y', '"+y', { desc = 'Copy (system clipboard)' })
vim.keymap.set('n', '<leader>p', '"+p', { desc = 'Paste (system clipboard)' })

-- line numbering
vim.keymap.set('n', '<leader>wr', util.toggle_relative_numbers, { desc = 'Workspace: [R]elative numbers (toggle)' })

-- TERMINAL MODE
vim.keymap.set('t', '<C-q>', '<cmd>qa<cr>', { desc = 'quit' })
vim.keymap.set('t', '<C-left>', '<Cmd>vertical resize -5<CR>', { desc = 'decrease window height' })
vim.keymap.set('t', '<C-right>', '<Cmd>vertical resize +5<CR>', { desc = 'increase window height' })
vim.keymap.set('t', '<C-down>', '<Cmd>horizontal resize -2<CR>', { desc = 'decrease window width' })
vim.keymap.set('t', '<C-up>', '<Cmd>horizontal resize +2<CR>', { desc = 'increase window width' })
vim.keymap.set('t', '<C-h>', [[<Cmd>wincmd h<CR>]], { desc = 'Move focus to the left window' })
vim.keymap.set('t', '<C-j>', [[<Cmd>wincmd j<CR>]], { desc = 'Move focus to the lower window' })
vim.keymap.set('t', '<C-k>', [[<Cmd>wincmd k<CR>]], { desc = 'Move focus to the upper window' })
vim.keymap.set('t', '<C-l>', [[<Cmd>wincmd l<CR>]], { desc = 'Move focus to the right window' })
vim.keymap.set('t', '<C-\\>', [[<C-\><C-n>]], { desc = 'normal mode' })
vim.keymap.set('t', '<C-u>', [[<C-\><C-n><C-u>]], { desc = 'scroll up' })

-- ----------------------------------------------------------------------------
-- PLUGINS
-- ----------------------------------------------------------------------------
-- LSP:       plugins/lsp.lua
-- TELESCOPE: plugins/telescope.lua
-- DEBUG:     plugins/debug.lua
-- WHICH KEY  plugin/which-key.lua

-- COMMENTS
-- ----------------------------------------------------------------------------
-- plugins/comment.lua
vim.keymap.set('n', '<leader>/', util.toggleComment, { desc = 'toggle line comment' })
vim.keymap.set('v', '<leader>/', util.toggleCommentBlock, { desc = 'toggle block comment' })

-- MASON, LAZY
-- ----------------------------------------------------------------------------
vim.keymap.set('n', '<leader>wm', '<cmd>Mason<CR>', { desc = 'Workspace: [M]ason' })
vim.keymap.set('n', '<leader>wl', '<cmd>Lazy<CR>', { desc = 'Workspace: [L]azy' })

-- file explorer
-- ----------------------------------------------------------------------------
-- plugins/nvim-tree.lua
vim.keymap.set('n', '<C-e>', '<Cmd>NvimTreeToggle<CR>', { desc = 'Toggle File Explorer' })

-- GIT:
-- ----------------------------------------------------------------------------
-- plugins/gitsigns.lua
vim.keymap.set('n', ']h', '<cmd>Gitsigns next_hunk<CR>', { desc = 'Git: next hunk' })
vim.keymap.set('n', '[h', '<cmd>Gitsigns prev_hunk<CR>', { desc = 'Git: prev hunk' })
vim.keymap.set('n', '<leader>gd', '<cmd>Gitsigns reset_hunk<CR>', { desc = 'Git: [D]elete hunk' })
vim.keymap.set('n', '<leader>gp', '<cmd>Gitsigns preview_hunk_inline<CR>', { desc = 'Git: [P]review hunk inline' })
vim.keymap.set('n', '<leader>gs', '<cmd>Gitsigns stage_hunk<CR>', { desc = 'Git: [S]tage hunk' })
vim.keymap.set('n', '<leader>gu', '<cmd>Gitsigns undo_stage_hunk<CR>', { desc = 'Git: [S]tage hunk' })
-- plugins/lazygit.lua

-- TERMINAL
-- ----------------------------------------------------------------------------
-- plugins/toggleterm.lua
vim.keymap.set('n', '<C-t>', '<cmd>ToggleTerm<CR>', { desc = 'Toggle Terminal' })
vim.keymap.set('t', '<C-t>', '<Cmd>ToggleTerm<CR>', { desc = 'Toggle Terminal' })
