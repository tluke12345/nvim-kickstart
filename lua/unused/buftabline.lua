-- Adds a 'tabline' that shows the current buffers (':ls')
return {
  'ap/vim-buftabline',
  config = function()
    -- Options
    -- ------------------------------------------------------------------------

    -- show the tabline
    -- 0: never, 1: 2 or more buffers, 2: always
    -- default 2 (always)
    vim.g.buftabline_show = 1

    -- add tab numbering
    -- 0: none, 1: buffer number, 2: ordinal number
    -- default 0 (none)
    vim.g.buftabline_numbers = 2

    -- add 'modified' indicator '+'
    -- default false
    vim.g.buftabline_indicators = true

    -- vertical line drawn between tabs
    -- default false
    --[[ vim.g.buftabline_separators = true ]]

    -- MAPPINGS
    -- ------------------------------------------------------------------------
    -- <leader>1 -> buffer 1
    -- <leaaer>2 -> buffer 2
    -- ...

    -- max number of 'bufline-mappings'
    -- 0: disabled, <number>: max amount
    -- default 10
    --[[ vim.g.buftabline_plug_max = 10 ]]

    -- use the mappings
    -- vim.keymap.set('n', '<leader>1', '<Plug>BufTabLine.Go(1)', { desc = 'Buffer 1' })
    -- vim.keymap.set('n', '<leader>2', '<Plug>BufTabLine.Go(2)', { desc = 'Buffer 2' })
    -- vim.keymap.set('n', '<leader>3', '<Plug>BufTabLine.Go(3)', { desc = 'Buffer 3' })
    -- vim.keymap.set('n', '<leader>4', '<Plug>BufTabLine.Go(4)', { desc = 'Buffer 4' })
    -- vim.keymap.set('n', '<leader>5', '<Plug>BufTabLine.Go(5)', { desc = 'Buffer 5' })
    -- vim.keymap.set('n', '<leader>6', '<Plug>BufTabLine.Go(6)', { desc = 'Buffer 6' })
    -- vim.keymap.set('n', '<leader>7', '<Plug>BufTabLine.Go(7)', { desc = 'Buffer 7' })
    -- vim.keymap.set('n', '<leader>8', '<Plug>BufTabLine.Go(8)', { desc = 'Buffer 8' })
    -- vim.keymap.set('n', '<leader>9', '<Plug>BufTabLine.Go(9)', { desc = 'Buffer 9' })
    -- vim.keymap.set('n', '<leader>0', '<Plug>BufTabLine.Go(10)', { desc = 'Buffer 10' })

    -- Colors
    -- ------------------------------------------------------------------------

    -- get list of all current color groups:
    -- :so $VIMRUNTIME/syntax/hitest.vim

    -- get list of vim default color groups:
    -- :help highlight-groups

    -- setting hightlight group
    -- (no lua api so call vimscript command)
    -- vim.cmd [[hi link <NewGroup> <ExistingGroup>]]

    -- Buffer shown in current window (default: TabLineSel)
    vim.cmd [[hi link BufTabLineCurrent PmenuSel]]

    -- Buffer shown in other window (default: PmenuSel)
    vim.cmd [[hi link BufTabLineActive DiffText]]

    -- Buffer not currently visible (default: TabLine)
    vim.cmd [[hi link BufTabLineHidden TabLine]]

    -- Empty area (default: TabLineFill)
    -- vim.cmd [[hi link BufTabLineFill TabLineFill]]

    -- same as linked but 'modified' (default BufTabLineCurrent)
    -- vim.cmd [[hi link BufTabLineModifiedCurrent BufTabLineCurrent]]

    -- same as linked but 'modified' (default BufTabLineActive)
    -- vim.cmd [[hi link BufTabLineModifiedActive BufTabLineActive]]

    -- same as linked but 'modified' (default BufTabLineHidden)
    -- vim.cmd [[hi link BufTabLineModifiedHidden BufTabLineHidden]]
  end,
}
