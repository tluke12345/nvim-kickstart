local utility = require 'utility'

-- Only use Copilot on WSL (Home)
if utility.is_wsl() == false then
  return {}
end

return {
  {
    'github/copilot.vim',
    config = function()
      vim.keymap.set('n', '<leader>wcd', '<Cmd>Copilot disable<CR>', { desc = 'Copilot: disable' })
      vim.keymap.set('n', '<leader>wce', '<Cmd>Copilot enable<CR>', { desc = 'Copilot: enable' })
      vim.keymap.set('n', '<leader>wcs', '<Cmd>Copilot status<CR>', { desc = 'Copilot: status' })
      vim.keymap.set('n', '<leader>wcp', '<Cmd>Copilot panel<CR>', { desc = 'Copilot: panel' })
    end,
  },
}
