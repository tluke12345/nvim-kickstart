# Neovim Configuration

## KEYBINDINGS

- general keybindings -> keybindings.lua

### plugin-specific keybindings

- which-key sections -> plugin/which-key.lua
- Lsp -> plugin/lsp.lua
- Telescope -> plugin/telescope.lua
- Terminal -> plugin/toggleterm.lua
- Git -> plugin/gitsigns.lua, plugin/lazygit.lua
- Debug -> plugin/debug.lua
- File Explorer -> plugin/nvim-tree.lua
- Comments -> plugin/comment.lua
- Copilot -> plugin/copilot.lua

## PLUGINS

### /plugin

Files in directory are auto-loaded by the start-up script.
These files should return a 'lazy' table.

### /unused

Unused plugins are kept here for reference.

## Language Servers / Linters / Formatters

### LSP

- auto-loaded -> plugins/lsp.lua ("ensured_installed" and "language_configurations")
- manually installed -> :LspInstall LANGUAGE

### Linters / Formatters / Diagnostics

- auto-loaded -> plugins/null-ls.lua ("ensured_installed")
- manually installed -> :Mason
