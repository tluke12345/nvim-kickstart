require 'options'
require 'keymaps'
require 'autocommands'

-- [[ Install `lazy.nvim` plugin manager ]]
-- ----------------------------------------------------------------------------
--    :Lazy               To check the current status of your plugins, run
--    :Lazy update        To update plugins, you can run
local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system {
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  }
end
vim.opt.rtp:prepend(lazypath)

-- [[ Configure and install plugins ]]
-- ----------------------------------------------------------------------------
-- All files in the 'plugins' directory are loaded as 'Lazy plugin tables'
-- to skip a file, change its extension to something other than '.lua'
local plugins = {}
for _, file in ipairs(vim.fn.readdir(vim.fn.stdpath 'config' .. '/lua/plugins', [[v:val =~ '\.lua$']])) do
  table.insert(plugins, require('plugins.' .. file:gsub('%.lua$', '')))
end

require('lazy').setup(plugins)

-- require('lazy').setup {
--   spec = 'plugins',
--   change_detection = { notify = false },
-- }
