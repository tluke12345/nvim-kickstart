local utility = require 'utility'
-- [[ Setting options ]]
-- See `:help vim.opt`
-- See `:help option-list`

-- General
vim.opt.mouse = ''        -- n (normal), v (visual), i (insert), c (command), h (all)
vim.opt.title = true      -- set terminal title to the filename and path
vim.opt.cmdheight = 0     -- hide command line unless needed
vim.opt.splitbelow = true -- splitting a new window below the current one
vim.opt.splitright = true -- splitting a new window at the right of the current one
vim.opt.laststatus = 3    -- global statusline
vim.opt.showmode = false  -- disable showing modes in command line
vim.opt.showtabline = 0   -- 0 (never), 1 (when tabs > 2), 2 (always)
vim.opt.pumheight = 10    -- height of the pop up menu
vim.opt.cursorline = true -- highlight the text line of the cursor
--  See `:help 'clipboard'`
vim.opt.clipboard = 'unnamed'
-- Minimal number of screen lines to keep above and below the cursor.
vim.opt.scrolloff = 10

-- Timing
vim.opt.updatetime = 300 -- length of time to wait before triggering the plugin
vim.opt.timeoutlen = 500 -- shorten key timeout length a little bit for which-key

-- Line Numbering
vim.opt.number = true         -- show numberline
vim.opt.relativenumber = true -- show relative numberline
vim.opt.signcolumn = 'yes'    -- always show the sign column
vim.opt.colorcolumn = '80,99'

-- Wrapping / Breaking
vim.opt.linebreak = true      -- wrap lines at 'breakat'
vim.opt.wrap = false          -- disable wrapping of lines longer than the width of window
vim.opt.preserveindent = true -- preserve indent structure as much as possible
vim.opt.breakindent = true    -- wrap indent to match  line start
vim.opt.copyindent = true     -- copy the previous indentation on autoindenting

-- Tabs
vim.opt.tabstop = 2      -- number of space in a tab
vim.opt.shiftwidth = 2   -- number of space inserted for indentation
vim.opt.expandtab = true -- enable the use of space in tab

-- Preview substitutions live, as you type!
vim.opt.inccommand = 'split'
vim.opt.virtualedit = 'block'                           -- allow going past end of line in visual block mode
vim.opt.completeopt = { 'menu', 'menuone', 'noselect' } -- Options for insert mode completion
vim.opt.fillchars = { eob = ' ' }                       -- disable `~` on nonexistent lines

-- Fold
vim.opt.foldenable = true                                      -- enable fold for nvim-ufo
vim.opt.foldlevel = 99                                         -- set high foldlevel for nvim-ufo
vim.opt.foldlevelstart = 99                                    -- start with all code unfolded
vim.opt.foldcolumn = vim.fn.has 'nvim-0.9' == 1 and '1' or nil -- show foldcolumn in nvim 0.9

-- Case
vim.opt.smartcase = true  -- case sensitive searching
vim.opt.ignorecase = true -- case insensitive searching
vim.opt.infercase = true  -- infer cases in keyword completion

-- Files
vim.opt.fileencoding = 'utf-8' -- file content encoding for the buffer
vim.opt.history = 100          -- number of commands to remember in a history table
vim.opt.undofile = true        -- enable persistent undo
vim.opt.writebackup = false    -- disable making a backup before overwriting a file

-- Sets how neovim will display certain whitespace in the editor.
--  See `:help 'list'`
--  and `:help 'listchars'`
--vim.opt.list = true
--vim.opt.listchars = { tab = '» ', trail = '·', nbsp = '␣' }

vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

----------------------- WSL specific settings -------------------------------
if utility.is_wsl() then
  -- Is WSL
  -- Set the clipboard to use WSL clipboard
  vim.g.clipboard = {
    name = 'WslClipboard',
    copy = {
      ['+'] = 'clip.exe',
      ['*'] = 'clip.exe',
    },
    paste = {
      ['+'] = 'powershell.exe -c [Console]::Out.Write($(Get-Clipboard -Raw).tostring().replace("`r", ""))',
      ['*'] = 'powershell.exe -c [Console]::Out.Write($(Get-Clipboard -Raw).tostring().replace("`r", ""))',
    },
    cache_enabled = 0,
  }
end

----------------------- TERM COLOR SUPPRT -----------------------------------
if utility.is_xterm_16color() then
  -- Is xterm-16color
  vim.opt.termguicolors = false
else
  -- Is not xterm-16color
  vim.opt.termguicolors = true
end

----------------------- TERM COLOR SUPPRT -----------------------------------
if vim.fn.executable '/usr/bin/fish' then
  vim.opt.shell = '/usr/bin/fish'
end
